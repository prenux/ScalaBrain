package main.scala.utils

import main.scala.utils.Matrix.Matrix
import main.scala.utils.Row.Row

import scala.util.Random

object Math {
  // Dot Product for 2 VECTORS
  def dotProd(a:Array[Double],b:Array[Double]):Double = {
    if (a.length != b.length) throw new ArithmeticException()
    var res = 0.0
    var i = 0
    while (i < a.length){
      res += a(i) * b(i)
      i += 1
    }
    res
  }

  def randVec(len:Int): Array[Double] = {
    val res = new Array[Double](len)
    var i = 0
    while (i < len) {
      res(i) = Random.nextDouble()
      i += 1
    }
    res
  }

  def map(func:Double => Double,vec: Array[Double]):Array[Double] = {
    var i = 0
    while (i < vec.length){
      i+=1
    }
    vec
  }

  def relu(matrix: Matrix):Matrix ={
    matrix.map(_.map(v => if (v > 0) v else 0))
  }

  def softmax(matrix: Matrix):Matrix = {
    val z_exp = matrix.map(_.map(v => math.exp(v)))
    val sum_z_exp = z_exp.map(_.sum).sum
    for (i <- z_exp) yield List(i.head / sum_z_exp)
  }

  def identity(matrix: Matrix):Matrix = matrix

//  >>> import math
//  >>> z = [1.0, 2.0, 3.0, 4.0, 1.0, 2.0, 3.0]
//  >>> z_exp = [math.exp(i) for i in z]
//  >>> print([round(i, 2) for i in z_exp])
//  [2.72, 7.39, 20.09, 54.6, 2.72, 7.39, 20.09]
//  >>> sum_z_exp = sum(z_exp)
//  >>> print(round(sum_z_exp, 2))
//  114.98
//  >>> softmax = [round(i / sum_z_exp, 3) for i in z_exp]
//  >>> print(softmax)
//    [0.024, 0.064, 0.175, 0.475, 0.024, 0.064, 0.175]

}
