package main.scala.brainparts

import utils.TrainingSet

class NeuralNet (nInputs:Int, nHidden:Int, nOutputs:Int,learn_rate:Double, hidden_layer_weights:Array[Double],
                 hidden_layer_bias:Double, output_layer_weights:Array[Double],
                 output_layer_bias:Double){
  var hidden_layer = new NeuronLayer(nInputs,nHidden, hidden_layer_bias)
  var output_layer = new NeuronLayer(nHidden,nOutputs, output_layer_bias)

  init_weights_from_inputs_to_hidden_layer_neurons(hidden_layer_weights)
  init_weights_from_hidden_layer_neurons_to_output_layer_neurons(output_layer_weights)

  def init_weights_from_inputs_to_hidden_layer_neurons(hidden_layer_weights:Array[Double]): Unit = {
    var weight_num = 0
    var i = 0
    while (i < hidden_layer.neurons.length) {
      var j = 0
      while (j < nInputs) {
        hidden_layer.neurons(i).weights(j) = hidden_layer_weights(weight_num)
        weight_num += 1
        j += 1
      }
      i += 1
    }
  }

  def init_weights_from_hidden_layer_neurons_to_output_layer_neurons(output_layer_weights:Array[Double]): Unit = {
    var weight_num = 0
    var i = 0
    while (i < output_layer.neurons.length) {
      var j = 0
      while (j < hidden_layer.neurons.length) {
        output_layer.neurons(i).weights(j) = output_layer_weights(weight_num)
        weight_num += 1
        j+=1
      }
      i+=1
    }
  }

  def inspect(): Unit = {
    print("------")
    print("* Inputs: " + nInputs)
    print("------")
    print("Hidden Layer")
    hidden_layer.inspect()
    print("------")
    print("* Output Layer")
    output_layer.inspect()
    print("------")
  }

  def feed_forward(inputs:Array[Double]): Array[Double] = {
    val hidden_layer_outputs = hidden_layer.feed_forward(inputs)
    output_layer.feed_forward(hidden_layer_outputs)
  }

  // Uses online learning, ie updating the weights after each training case
  def train(training_inputs:Array[Double], training_outputs:Array[Double]): Unit = {
    feed_forward(training_inputs)

    // 1. Output neuron deltas
    val pd_errors_wrt_output_neuron_total_net_input = Array.fill[Double](output_layer.neurons.length)(0)
    var i = 0
    while (i < output_layer.neurons.length) {
      // ∂E/∂zⱼ
      pd_errors_wrt_output_neuron_total_net_input(i) = output_layer.neurons(i).tot_net_input_pd_error(training_outputs(i))
      i += 1
    }

    // 2. Hidden neuron deltas
    val pd_errors_wrt_hidden_neuron_total_net_input = Array.fill[Double](hidden_layer.neurons.length)(0)
    i = 0
    while (i < hidden_layer.neurons.length) {
      // We need to calculate the derivative of the error with respect to the output of each hidden layer neuron
      // dE/dyⱼ = Σ ∂E/∂zⱼ * ∂z/∂yⱼ = Σ ∂E/∂zⱼ * wᵢⱼ
      var d_error_wrt_hidden_neuron_output = 0.0
      var j = 0
      while (j < output_layer.neurons.length) {
        d_error_wrt_hidden_neuron_output += pd_errors_wrt_output_neuron_total_net_input(j) * output_layer.neurons(j).weights(i)
        j += 1
      }
      // ∂E/∂zⱼ = dE/dyⱼ * ∂zⱼ/∂
      pd_errors_wrt_hidden_neuron_total_net_input(i) = d_error_wrt_hidden_neuron_output * hidden_layer.neurons(i).calc_tot_net_input_pd_wrt_input()
      i += 1
    }

    // 3. Update output neuron weights
    i = 0
    while (i < output_layer.neurons.length) {
      var j = 0
      while (j < output_layer.neurons(i).weights.length) {
        // ∂Eⱼ/∂wᵢⱼ = ∂E/∂zⱼ * ∂zⱼ/∂wᵢⱼ
        val pd_error_wrt_weight = pd_errors_wrt_output_neuron_total_net_input(i) * output_layer.neurons(i).calc_tot_net_input_pd_wrt_weight(j)
        // Δw = α * ∂Eⱼ/∂wᵢ
        output_layer.neurons(i).weights(j) -= learn_rate * pd_error_wrt_weight
        j += 1
      }
      i += 1
    }
    // 4. Update hidden neuron weights
    i = 0
    while (i < hidden_layer.neurons.length) {
      var j = 0
      while (j < hidden_layer.neurons(i).weights.length) {
        // ∂Eⱼ/∂wᵢ = ∂E/∂zⱼ * ∂zⱼ/∂wᵢ
        val pd_error_wrt_weight = pd_errors_wrt_hidden_neuron_total_net_input(i) * hidden_layer.neurons(i).calc_tot_net_input_pd_wrt_weight(j)
        // Δw = α * ∂Eⱼ/∂wᵢ
        hidden_layer.neurons(i).weights(j) -= learn_rate * pd_error_wrt_weight
        j += 1
      }
      i += 1
    }
  }

  def calculate_total_error(training_sets:Array[TrainingSet]): Double = {
    var total_error = 0.0
    var i = 0
    while(i < training_sets.length) {
      val training_inputs = training_sets(i).inputs
      val training_outputs = training_sets(i).outputs
      feed_forward(training_inputs)
      var j = 0
      while (j < training_outputs.length) {
        total_error += output_layer.neurons(j).calculate_error(training_outputs(j))
        j+=1
      }
      i+=1
    }
    total_error
  }
}
