package main.scala.brainparts

import main.scala.utils.Matrix.Matrix
import main.scala.utils.RichMatrix
import main.scala.utils.Math._
import main.scala.utils._

class DaBrain(weights_param:List[Matrix], biases:Matrix,
              hidden_act:Matrix => Matrix = relu, output_act:Matrix => Matrix = softmax) {

  val weights: List[RichMatrix] = weights_param.map(m => RichMatrix(m))
  
  def fprop(X:RichMatrix):RichMatrix = {
    var act = X // (d x n)
    var i = 0
    val len = weights.length
    while (i < len){
      var pa =  act * weights(i)
      // b is per neuron
      pa = for(row <- pa) yield (row, biases(i)).zipped.map(_ + _)

      // b is SHARED across layer
      //val b = biases(i).head
      //pa = pa.map(row => row.map(el => el + b))
      if(i < len -1) act = RichMatrix(hidden_act(pa))
      else act = RichMatrix(output_act(pa))
      i+=1
    }
    act
  }
}
