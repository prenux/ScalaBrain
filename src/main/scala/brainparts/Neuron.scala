package main.scala.brainparts

class Neuron(nInputs:Int, var bias:Double) {
  var weights = Array.fill[Double](nInputs)(0)
  var inputs:Array[Double] = _
  var output:Double = _

  //Logistic regression to squash output
  def squash(total_net_input:Double):Double = 1/(1+math.exp(-total_net_input))

  def calculate_total_net_input():Double = {
    var total = 0.0
    var i = 0
    while(i < inputs.length){
      total += inputs(i) * weights(i)
      i += 1
    }
    total + bias
  }

  def calculate_output(inputs:Array[Double]):Double = {
    this.inputs = inputs
    this.output = squash(calculate_total_net_input())
    output
  }

  // Mean Square error for neuron perhaps change it for other loss function
  def calculate_error(target_output:Double): Double = 0.5 * Math.pow(target_output - output,2)

  // The partial derivate of the error with respect to actual output then is calculated by:
  // = 2 * 0.5 * (target output - actual output) ^ (2 - 1) * -1
  // = -(target output - actual output)
  //
  // The Wikipedia article on backpropagation [1] simplifies to the following, but most other learning material does not [2]
  // = actual output - target output
  //
  // Alternative, you can use (target - output), but then need to add it during backpropagation [3]
  //
  // Note that the actual output of the output neuron is often written as yⱼ and target output as tⱼ so:
  // = ∂E/∂yⱼ = -(tⱼ - yⱼ)
  def output_pd_error(target_output:Double): Double = -(target_output - output)

  // The total net input into the neuron is squashed using logistic function to calculate the neuron's output:
  // yⱼ = φ = 1 / (1 + e^(-zⱼ))
  // Note that where ⱼ represents the output of the neurons in whatever layer we're looking at and ᵢ represents the layer below it
  //
  // The derivative (not partial derivative since there is only one variable) of the output then is:
  // dyⱼ/dzⱼ = yⱼ * (1 - yⱼ)
  def calc_tot_net_input_pd_wrt_input(): Double = output * (1 - output)

  // Determine how much the neuron's total input has to change to move closer to the expected output
  //
  // Now that we have the partial derivative of the error with respect to the output (∂E/∂yⱼ) and
  // the derivative of the output with respect to the total net input (dyⱼ/dzⱼ) we can calculate
  // the partial derivative of the error with respect to the total net input.
  // This value is also known as the delta (δ) [1]
  // δ = ∂E/∂zⱼ = ∂E/∂yⱼ * dyⱼ/dzⱼ
  def tot_net_input_pd_error(target_output:Double):Double = {
    output_pd_error(target_output) * calc_tot_net_input_pd_wrt_input()
  }

  // The total net input is the weighted sum of all the inputs to the neuron and their respective weights:
  // = zⱼ = netⱼ = x₁w₁ + x₂w₂ ...
  //
  // The partial derivative of the total net input with respective to a given weight (with everything else held constant) then is:
  // = ∂zⱼ/∂wᵢ = some constant + 1 * xᵢw₁^(1-0) + some constant ... = xᵢ
  def calc_tot_net_input_pd_wrt_weight(index:Int) = inputs(index)

}
