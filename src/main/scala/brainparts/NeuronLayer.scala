package main.scala.brainparts

class NeuronLayer(nInputs:Int,nNeurons:Int,bias:Double) {
  // Every neuron in a layer shares the same bias
  var neurons = new Array[Neuron](nNeurons)
  var i = 0
  while(i < nNeurons) {
    neurons(i) = new Neuron(nInputs,bias)
    i += 1
  }

  def inspect(): Unit = {
    println("Neurons: " + neurons.length)
    var i = 0
    while (i < neurons.length) {
      println("\tNeuron: " + i)
      println("\t\tBias: " + neurons(i).bias)
      var j = 0
      while (j < neurons(i).weights.length) {
        println("\t\tWeight: " + neurons(i).weights(j))
        j += 1
      }
      i += 1
    }
  }

  def feed_forward(inputs:Array[Double]): Array[Double] = {
    val outputs = new Array[Double](neurons.length)
    var i = 0
    while(i < neurons.length){
      outputs(i) = neurons(i).calculate_output(inputs)
      i+=1
    }
    outputs
  }

  def get_outputs(): Array[Double] = {
    val outputs = new Array[Double](neurons.length)
    var i = 0
    while(i < neurons.length) {
      outputs(i) = neurons(i).output
      i+=1
    }
    outputs
  }
}
