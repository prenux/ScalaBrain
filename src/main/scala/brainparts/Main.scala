package main.scala.brainparts

import main.scala.utils.Matrix.Matrix
import main.scala.utils.RichMatrix
import main.scala.utils.Math._

object Main {

  def test_matrix(): Unit = {
    var m = RichMatrix(List(
      List(1,0,0,0),
      List(0,2,0,0),
      List(0,0,3,0),
      List(0,0,0,-4)))
    val x = m*m
    println(x)
    println(x.getClass())
    println(relu(m.m))
    val n = RichMatrix(List(
      List(1),
      List(2),
      List(3),
      List(10)
    ))
    println(softmax(n.m))
  }

  def main(args: Array[String]): Unit = {
    //test_matrix()
    test_on_xor()
  }

  def test_on_xor():Unit = {
    val weights = List( List(List(1.0,1.0),List(1.0,1.0)), List(List(1.0),List(-2.0)))
    val biases = List(List(0.0, -1.0), List(0.0, 0.0))
    val gbrassard = new DaBrain(weights, biases, relu, identity)
    println(gbrassard.fprop(RichMatrix(List(List(0.0,0.0),List(0.0,1.0),List(1.0,0.0),List(1.0,1.0)))))
  }

  def test_nn(): Unit = {
    println("Creating Neural Network")
    val nn = new NeuralNet(2, 2, 2, 0.5, Array(0.15, 0.2, 0.25, 0.3),
      0.35, Array(0.4, 0.45, 0.5, 0.55), 0.6)
    println("Done")
    val training_set= new utils.TrainingSet(Array(0.05, 0.1), Array(0.01, 0.99))
    var i = 0
    println("Going to train!")
    while(i < 10000) {
      nn.train(Array(0.05, 0.1), Array(0.01, 0.99))
      println("" + i + " " + nn.calculate_total_error(Array(training_set)))
      i += 1
    }
    println("Done")
  }

}
